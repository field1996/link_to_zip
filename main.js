function getClickHandler() {
    return function(info) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {greeting: "hello"}, function(response) {
                console.log(response);
            });
        });
    };
}

var parentId = chrome.contextMenus.create({
    "title" : "Link_to_zip",
    "type" : "normal",
    "contexts" : ["all"],
    "onclick" : getClickHandler()
});