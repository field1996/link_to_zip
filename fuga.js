var table;
var tableLink;
var list;
var selection = function(){
    var sel = window.getSelection();
    var selectionDOM = sel.getRangeAt(0).cloneContents();
    var atag = selectionDOM.querySelectorAll('a');
    list = new Array();
    table = new Array();
    for(var item of atag){
        var row = new Array();
        var link = item.href;
        var title = item.innerHTML;
        list.push(link);
        row.push(title);
        row.push(link);
        table.push(row);
    }

    if(!sel.rangeCount) return;
    createCSV(table);
    return list;
};

var createCSV = (function() {
    var tableToCsvString = function(table) {
        var str = '\uFEFF';
        for (var i = 0, imax = table.length - 1; i <= imax; ++i) {
            var row = table[i];
            for (var j = 0, jmax = row.length - 1; j <= jmax; ++j) {
                str += '"' + row[j].replace('"', '""') + '"';
                if (j !== jmax) {
                    str += ',';
                }
            }
            str += '\n';
        }
        return str;
    };

    var createDataUriFromString = function(str) {
        return 'data:text/csv,' + encodeURIComponent(str);
    };

    return function(table, filename) {
        if (!filename) {
            filename = 'output.csv';
        }
        tableLink = createDataUriFromString(tableToCsvString(table));
        batchDownload(list);
    };
})();

var isSupported = !!(window.FormData && window.Blob && window.ArrayBuffer);

var downloadFile = function (url) {
    var xhr = new XMLHttpRequest(),
        deferred = new $.Deferred();

    xhr.addEventListener('load', function() {
        deferred.resolve(xhr);
    });

    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.send();

    return deferred.promise()
};

var batchDownload = function(urlList) {
    var zip = new JSZip(),
        deferreds = [];

    var csv = downloadFile(tableLink).done(function(xhr) {
        filename = 'files.csv';
        zip.file(filename, xhr.response);
    });
    deferreds.push(deferred);

    for (var i = 0; i < urlList.length; i += 1) {
        var deferred = (function(index) {
            var url = urlList[index];
            var filename_ex = url.match(".+/(.+?)([\?#;].*)?$")[1];
            if(filename_ex.match("^[\.]")){
                return null;
            }
            if(filename_ex.match("\.html")){
                return null;
            }
            return downloadFile(url).done(function(xhr) {
                filename = filename_ex;
                zip.file(filename, xhr.response);
            })
        })(i);
        if(deferred != null){
            deferreds.push(deferred);
        }
    }
    $.when.apply($, deferreds).done(function() {
        zip.generateAsync({type:"blob"})
            .then(function(deferreds) {
                saveAs(deferreds, "LinkFiles.zip");
            });
    });
};

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        console.log(sender.tab ?
            "from a content script:" + sender.tab.url :
            "from the extension");
        if (request.greeting == "hello"){
            sendResponse(selection())
        }
    });